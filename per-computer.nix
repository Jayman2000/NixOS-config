#  Copyright 2012 Jason Yundt. See the COPYING file at the top-level directory of this distribution and at https://gitlab.com/Jayman2000/NixOS-config/blob/master/COPYING.


# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "Jason-Laptop-Linux"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Needed for networking under kde
  networking.networkmanager.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    w3m git
  ];

  powerManagement.powertop.enable = true;

  # List services that you want to enable:

  # Enable touchpad support.
  services.xserver.libinput.enable = true;
}
